from urllib.parse import urlunparse

from django.contrib.postgres.fields import JSONField
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.template.loader import get_template

TYPE_KITCHEN = 'KT'
TYPE_CLIENT = 'CL'
CHECK_TYPES = (
    (TYPE_KITCHEN, 'kitchen',),
    (TYPE_CLIENT, 'client',),
)


class Printer(models.Model):
    name = models.CharField(max_length=100)
    api_key = models.CharField(max_length=128, unique=True, null=False, blank=False)
    check_type = models.CharField(max_length=2, choices=CHECK_TYPES)
    point_id = models.IntegerField()

    def __str__(self):
        return self.name

    @staticmethod
    def auth_printer(api_key):
        """Авторизация принтера
        :param str api_key: API ключ принтера
        :return: Принтер
        """
        if api_key:
            try:
                printer = Printer.objects.get(api_key=api_key)
            except ObjectDoesNotExist:
                printer = None
        else:
            printer = None
        return printer


class Check(models.Model):
    STATUS_NEW = 'N'
    STATUS_RENDERED = 'R'
    STATUS_PRINTED = 'P'
    STATUSES = (
        (STATUS_NEW, 'new'),
        (STATUS_RENDERED, 'rendered'),
        (STATUS_PRINTED, 'printed'),
    )

    printer_id = models.ForeignKey(Printer, related_name='checks')
    type = models.CharField(max_length=2, choices=CHECK_TYPES)
    order = JSONField(null=True, blank=True)
    status = models.CharField(max_length=1, choices=STATUSES)
    pdf_file = models.FileField(blank=True, upload_to='pdf')

    @staticmethod
    def get_rendered_checks(printer, host):
        """
        Возвращает dict с ссылками на pdf готовых чеков.

        :param Printer printer: Принтер
        :param host:
        :return:
        """
        checks = Check.objects.filter(printer_id=printer, status=Check.STATUS_RENDERED)
        checks_data = {
            'checks': []
        }
        for check in checks:
            check_url_query = 'order_id={0}&format={1}&api_key={2}'.format(check.order['order']['id'], 'pdf',
                                                                           printer.api_key)
            check_url = urlunparse(('http', host, '/check', None, check_url_query, None))
            checks_data['checks'].append({'id': check.id,
                                          'url': check_url})
        return checks_data

    def create_html(self):
        """Создает html версию чека

        :return: html версия чека
        """
        if self.type == TYPE_CLIENT:
            template = get_template('client_check.html')
        elif self.type == TYPE_KITCHEN:
            template = get_template('kitchen_check.html')
        context = {'data': self.order}
        return template.render(context)
