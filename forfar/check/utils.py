from django.core.exceptions import ObjectDoesNotExist

from .models import Check, Printer


def get_point_printer(order_point, check_type):
    """
    Получаем список привязанных к точке принтеров

    :param int order_point: точка к которой привязан принтер
    :param str check_type: тип чека
    :return: Принтер или None при отсутствие принтера определенного типа
    """
    try:
        point_printer = Printer.objects.get(point_id=order_point, check_type=check_type)
        return point_printer
    except ObjectDoesNotExist:
        return None


def create_check_entry(printer, order_data, check_type):
    """
    Создает запись чека и возвращает ее.

    :param Printer printer: принтер
    :param dict order_data: информация о заказе
    :param str check_type: тип чека
    :return: чек
    """
    check = Check(printer_id=printer, order=order_data, type=check_type, status=Check.STATUS_NEW)
    check.save()
    return check
