from json import loads

from django.http import JsonResponse, HttpResponse, FileResponse
from django.views.decorators.http import require_http_methods

from .models import Check, Printer, CHECK_TYPES
from .tasks import create_pdf
from .utils import get_point_printer, create_check_entry


@require_http_methods(["POST"])
def create_checks(request):
    # Получение query params
    order_json = request.body.decode('utf-8')
    order_data = loads(order_json)
    order_point = order_data['order']['point_id']
    order_id = order_data['order']['id']

    if Printer.objects.filter(point_id=order_point).count() < 1:
        return JsonResponse({'error': 'Для данной точки не настроено ни одного принтера'}, status=400)
    if Check.objects.filter(order__order__id=order_id).count() > 0:
        return JsonResponse({'error': 'Для данного заказа уже созданы чеки'}, status=400)

    # создание чеков и отправка задания на генерацию pdf версии
    for check_type in CHECK_TYPES:
        printer = get_point_printer(order_point, check_type[0])
        if not printer:
            continue
        current_check = create_check_entry(printer, order_data, check_type[0])
        html_check = current_check.create_html()
        create_pdf.delay(current_check, html_check)
    return JsonResponse({'ok': 'Чеки успешно созданы'}, status=200)


@require_http_methods(["GET"])
def new_checks(request):
    api_key = request.GET.get('api_key', '')
    printer = Printer.auth_printer(api_key)
    if not printer:
        return JsonResponse({'error': 'Ошибка авторизации'}, status=401)
    checks_data = Check.get_rendered_checks(printer, request.get_host())
    return JsonResponse(checks_data)


@require_http_methods(["GET"])
def check(request):
    # Получение query params
    api_key = request.GET.get('api_key', '')
    check_format = request.GET.get('format', '')
    order_id = int(request.GET.get('order_id', ''))

    # Авторизация принтера
    printer = Printer.auth_printer(api_key)
    if not printer:
        return JsonResponse({'error': 'Ошибка авторизации'}, status=401)

    # Проверка наличия чеков
    checks = Check.objects.filter(order__order__id=order_id)
    if checks.count() < 1:
        return JsonResponse({'error': 'Для данного заказа нет чеков'}, status=400)

    # Создание ответа
    current_check = checks.get(printer_id=printer)
    if check_format == 'html':
        html_check = current_check.create_html()
        return HttpResponse(html_check)
    elif check_format == 'pdf':
        if current_check.status == Check.STATUS_NEW:
            return JsonResponse({'error': 'Для данного заказа не сгенерирован чек в формате PDF'}, status=400)
        current_check.status = Check.STATUS_PRINTED
        current_check.save()
        return FileResponse(current_check.pdf_file, content_type='application/pdf')
