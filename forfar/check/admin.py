from django.contrib import admin

from .models import Printer, Check


class PrinterAdmin(admin.ModelAdmin):
    list_display = ('name', 'api_key', 'check_type', 'point_id')


class CheckAdmin(admin.ModelAdmin):
    list_display = ('id', 'printer_id', 'type', 'status', 'pdf_file')
    list_filter = ('printer_id', 'type', 'status')


admin.site.register(Printer, PrinterAdmin)
admin.site.register(Check, CheckAdmin)
