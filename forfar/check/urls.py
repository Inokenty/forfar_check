from django.conf.urls import url

from .views import check, create_checks, new_checks

urlpatterns = [
    url(r'^check$', check, name='check'),
    url(r'^create_checks$', create_checks, name='create_checks'),
    url(r'^new_checks$', new_checks, name='new_checks'),
]
