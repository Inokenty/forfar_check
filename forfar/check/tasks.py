import json
import tempfile
from base64 import b64encode

import requests
from django.conf import settings
from django.core.files import File
from django_rq import job

from .models import Check

HTMLTOPDF_URL = settings.HTMLTOPDF_URL


@job
def create_pdf(check, html_check):
    data = {
        'contents': b64encode(bytes(html_check, 'utf-8')).decode('ascii'),
    }
    headers = {
        'Content-Type': 'application/json',
    }
    response = requests.post(HTMLTOPDF_URL, data=json.dumps(data), headers=headers)
    filename = '{0}_{1}.pdf'.format(check.order['order']['id'], check.get_type_display())
    with tempfile.TemporaryFile() as temp_file:
        temp_file.write(response.content)
        check.pdf_file.save(filename, File(temp_file))
    check.status = Check.STATUS_RENDERED
    check.save()
