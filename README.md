# Форфар
## Запуск
```bash
pip install -r requirements.txt
docker-compose up
python forfar/manage.py migrate
python forfar/manage.py loaddata initial_data.json
python forfar/manage.py runserver
python forfar/manage.py rqworker default
```
## Примечания
1. Шаблон чека для кухни не влезает в одну страницу из-за выставленной высоты body
2. Ответ /new_checks в примере выдает неверную ссылку